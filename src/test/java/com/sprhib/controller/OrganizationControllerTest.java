package com.sprhib.controller;

import com.sprhib.dao.OrganizationDAO;
import com.sprhib.dao.TeamDAO;
import com.sprhib.init.BaseTestConfig;
import com.sprhib.model.Organization;
import com.sprhib.model.Team;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import static org.junit.Assert.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Created by indrekruubel on 25/11/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@Transactional
@ContextConfiguration(classes=BaseTestConfig.class)
public class OrganizationControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    private OrganizationDAO organizationDAO;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void addPageRender() throws Exception {
        mockMvc.perform(get("/organization/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-organization-form"));
    }


    @Test
    public void listPageRender() throws Exception {
        mockMvc.perform(get("/organization/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("list-of-organizations"));
    }

    @Test
    public void deletePage() throws Exception {

        // First save some team
        Organization organization = new Organization();
        organization.setName("Test");
        organizationDAO.addOrganization(organization);

        mockMvc.perform(get(String.format("/organization/delete/%s", organization.getId())))
                .andExpect(status().isOk())
                .andExpect(view().name("home"));

        Organization organizationDeleted = organizationDAO.getOrganization(organization.getId());
        assertNull(organizationDeleted);

    }


}
