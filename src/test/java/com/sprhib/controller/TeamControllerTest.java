package com.sprhib.controller;

import com.sprhib.dao.TeamDAO;
import com.sprhib.init.BaseTestConfig;
import com.sprhib.model.Team;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Created by indrekruubel on 25/11/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@Transactional
@ContextConfiguration(classes=BaseTestConfig.class)
public class TeamControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    private TeamDAO teamDAO;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void addPageRender() throws Exception {
        mockMvc.perform(get("/team/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-team-form"));
    }


    @Test
    public void listPageRender() throws Exception {
        mockMvc.perform(get("/team/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("list-of-teams"));
    }

    @Test
    public void deletePage() throws Exception {

        // First save some team
        Team team = new Team();
        team.setName("Test");
        team.setRating(3);
        teamDAO.addTeam(team);

        mockMvc.perform(get(String.format("/team/delete/%s", team.getId())))
                .andExpect(status().isOk())
                .andExpect(view().name("home"));

        Team teamDeleted = teamDAO.getTeam(team.getId());
        assertNull(teamDeleted);

    }


}
