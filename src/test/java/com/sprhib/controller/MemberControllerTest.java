package com.sprhib.controller;

import com.sprhib.dao.MemberDAO;
import com.sprhib.init.BaseTestConfig;
import com.sprhib.model.Member;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.junit.Assert.*;

/**
 * Created by indrekruubel on 25/11/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@Transactional
@ContextConfiguration(classes=BaseTestConfig.class)
public class MemberControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    private MemberDAO memberDAO;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void addPageRender() throws Exception {
        mockMvc.perform(get("/member/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-member-form"));
    }


    @Test
    public void listPageRender() throws Exception {
        mockMvc.perform(get("/member/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("list-of-members"));
    }

    @Test
    public void deleteMember() throws Exception {

        // First save some member
        Member member = new Member();
        member.setName("Test");
        member.setRating(3);
        memberDAO.addMember(member);

        mockMvc.perform(get(String.format("/member/delete/%s", member.getId())))
                .andExpect(status().isOk())
                .andExpect(view().name("home"));

        Member memberDeleted = memberDAO.getMember(member.getId());
        assertNull(memberDeleted);

    }


}
