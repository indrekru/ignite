package com.sprhib.page;

import com.sprhib.init.BaseTestConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by interaal on 29.11.14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BaseTestConfig.class)
@WebAppConfiguration
public class OrganizationPageTest {

    private Site site;
    private WebDriver driver;

    // Helper variables
    private String lastCreatedItemName = null;

    @Before
    public void setup(){
        driver = new FirefoxDriver();
        site = new Site(driver);
    }

    @After
    public void close(){
        driver.close();
    }

    @Test
    public void add(){

        OrganizationAddPage page = site.getOrganizationPage().getOrganizationAddPage(true);
        assertEquals("Add organization", page.getTitle());

        String uniqueName = String.format("_TEST-SELENIUM-%s", page.getRandom());
        lastCreatedItemName = uniqueName;
        page.setName(uniqueName);
        page.clickAdd();
        assertTrue(page.pageContains("Organization was successfully added"));

        // Cleanup
        cleanup(lastCreatedItemName);
        assertTrue(page.pageContains("Organization was successfully deleted"));
    }

    @Test
    public void edit() throws InterruptedException {
        // To edit something, let's create it first
        create();
        // Adding failed, fail yourself
        if(lastCreatedItemName == null){
            fail();
        }

        // Test
        OrganizationListPage page = site.getOrganizationPage().getOrganizationListPage(true);
        page.clickEditWithRowContaining(lastCreatedItemName);
        assertEquals("Edit organization", page.getTitle());
        OrganizationEditPage editPage = site.getOrganizationPage().getOrganizationEditPage(false);
        lastCreatedItemName.concat("_");
        editPage.setName(lastCreatedItemName);
        editPage.clickEdit();
        assertTrue(page.pageContains("Organization was successfully edited"));

        // Cleanup
        cleanup(lastCreatedItemName);
        assertTrue(page.pageContains("Organization was successfully deleted"));
    }

    @Test
    public void delete(){
        create();
        // Adding failed, fail yourself
        if(lastCreatedItemName == null){
            fail();
        }

        OrganizationListPage page = site.getOrganizationPage().getOrganizationListPage(true);
        page.clickDeleteWithRowContaining(lastCreatedItemName);
        assertTrue(page.pageContains("Organization was successfully deleted"));

    }

    /**
     * Helper functions
     */
    private void create(){
        OrganizationAddPage page = site.getOrganizationPage().getOrganizationAddPage(true);
        String uniqueName = String.format("_TEST-SELENIUM-%s", page.getRandom());
        lastCreatedItemName = uniqueName;
        page.setName(uniqueName);
        page.clickAdd();
    }

    private void cleanup(String name){
        OrganizationListPage page = site.getOrganizationPage().getOrganizationListPage(true);
        page.clickDeleteWithRowContaining(name);
    }

}
