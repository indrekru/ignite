package com.sprhib.page;

import com.sprhib.init.BaseTestConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

/**
 * Created by interaal on 29.11.14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BaseTestConfig.class)
@WebAppConfiguration
public class MemberPageTest {

    private Site site;
    private WebDriver driver;

    // Helper variables
    private String lastCreatedItemName = null;

    @Before
    public void setup(){
        driver = new FirefoxDriver();
        site = new Site(driver);
    }

    @After
    public void close(){
        driver.close();
    }

    @Test
    public void add(){

        MemberAddPage page = site.getMemberPage().getMemberAddPage(true);
        assertEquals("Add team member", page.getTitle());

        String uniqueName = String.format("_TEST-SELENIUM-%s", page.getRandom());
        lastCreatedItemName = uniqueName;
        page.setName(uniqueName);
        page.setRating(1);
        page.selectFirstTeam();
        page.clickAdd();
        assertTrue(page.pageContains("Member was successfully added"));

        // Cleanup
        cleanup(lastCreatedItemName);
        assertTrue(page.pageContains("Member was successfully deleted"));
    }

    @Test
    public void edit() throws InterruptedException {
        // To edit something, let's create it first
        create();
        // Adding failed, fail yourself
        if(lastCreatedItemName == null){
            fail();
        }

        // Test
        MemberListPage page = site.getMemberPage().getMemberListPage(true);
        page.clickEditWithRowContaining(lastCreatedItemName);
        assertEquals("Edit team member", page.getTitle());
        MemberEditPage editPage = site.getMemberPage().getMemberEditPage(false);
        lastCreatedItemName.concat("_");
        editPage.setName(lastCreatedItemName);
        editPage.setRating(4);
        editPage.clickEdit();
        assertTrue(page.pageContains("Member was successfully edited"));

        // Cleanup
        cleanup(lastCreatedItemName);
        assertTrue(page.pageContains("Member was successfully deleted"));
    }

    @Test
    public void delete(){
        create();
        // Adding failed, fail yourself
        if(lastCreatedItemName == null){
            fail();
        }

        MemberListPage page = site.getMemberPage().getMemberListPage(true);
        page.clickDeleteWithRowContaining(lastCreatedItemName);
        assertTrue(page.pageContains("Member was successfully deleted"));

    }

    /**
     * Helper functions
     */
    private void create(){
        MemberAddPage page = site.getMemberPage().getMemberAddPage(true);
        String uniqueName = String.format("_TEST-SELENIUM-%s", page.getRandom());
        lastCreatedItemName = uniqueName;
        page.setName(uniqueName);
        page.setRating(1);
        page.clickAdd();
    }

    private void cleanup(String name){
        MemberListPage page = site.getMemberPage().getMemberListPage(true);
        page.clickDeleteWithRowContaining(name);
    }

}
