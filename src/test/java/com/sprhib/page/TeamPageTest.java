package com.sprhib.page;

import com.sprhib.page.TeamAddPage;
import com.sprhib.page.TeamEditPage;
import com.sprhib.page.TeamListPage;
import com.sprhib.page.Site;
import com.sprhib.init.BaseTestConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

/**
 * Created by interaal on 29.11.14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BaseTestConfig.class)
@WebAppConfiguration
public class TeamPageTest {

    private Site site;
    private WebDriver driver;

    // Helper variables
    private String lastCreatedItemName = null;

    @Before
    public void setup(){
        driver = new FirefoxDriver();
        site = new Site(driver);
    }

    @After
    public void close(){
        driver.close();
    }

    @Test
    public void add(){

        TeamAddPage page = site.getTeamPage().getTeamAddPage(true);
        assertEquals("Add team", page.getTitle());

        String uniqueName = String.format("_TEST-SELENIUM-%s", page.getRandom());
        lastCreatedItemName = uniqueName;
        page.setName(uniqueName);
        page.setRating(1);
        page.clickAdd();
        assertTrue(page.pageContains("Team was successfully added"));

        // Cleanup
        cleanup(lastCreatedItemName);
        assertTrue(page.pageContains("Team was successfully deleted"));
    }

    @Test
    public void edit() throws InterruptedException {
        // To edit something, let's create it first
        create();
        // Adding failed, fail yourself
        if(lastCreatedItemName == null){
            fail();
        }

        // Test
        TeamListPage page = site.getTeamPage().getTeamListPage(true);
        page.clickEditWithRowContaining(lastCreatedItemName);
        assertEquals("Edit team", page.getTitle());
        TeamEditPage editPage = site.getTeamPage().getTeamEditPage(false);
        lastCreatedItemName.concat("_");
        editPage.setName(lastCreatedItemName);
        editPage.setRating(4);
        editPage.clickEdit();
        assertTrue(page.pageContains("Team was successfully edited"));

        // Cleanup
        cleanup(lastCreatedItemName);
        assertTrue(page.pageContains("Team was successfully deleted"));
    }

    @Test
    public void delete(){
        create();
        // Adding failed, fail yourself
        if(lastCreatedItemName == null){
            fail();
        }

        TeamListPage page = site.getTeamPage().getTeamListPage(true);
        page.clickDeleteWithRowContaining(lastCreatedItemName);
        assertTrue(page.pageContains("Team was successfully deleted"));

    }

    /**
     * Helper functions
     */
    private void create(){
        TeamAddPage page = site.getTeamPage().getTeamAddPage(true);
        String uniqueName = String.format("_TEST-SELENIUM-%s", page.getRandom());
        lastCreatedItemName = uniqueName;
        page.setName(uniqueName);
        page.setRating(1);
        page.clickAdd();
    }

    private void cleanup(String name){
        TeamListPage page = site.getTeamPage().getTeamListPage(true);
        page.clickDeleteWithRowContaining(name);
    }

}
