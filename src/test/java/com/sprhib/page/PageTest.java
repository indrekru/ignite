package com.sprhib.page;

import com.sprhib.init.BaseTestConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by interaal on 29.11.14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BaseTestConfig.class)
@WebAppConfiguration
public class PageTest {

    private Site site;
    private WebDriver driver;

    @Before
    public void setup(){
        driver = new FirefoxDriver();
        site = new Site(driver);
    }

    @After
    public void close(){
        driver.close();
    }

    @Test
    public void clickPages() throws Exception {
        FrontPage page = site.getFrontPage(true);
        assertEquals("Ignite test assignment by Indrek Ruubel", page.getTitle());

        page.click("Organizations");
        page.click("Add");
        assertEquals("Add organization", page.getTitle());

        page.click("Organizations");
        page.click("List");
        assertEquals("List of organizations", page.getTitle());

        page.click("Teams");
        page.click("Add");
        assertEquals("Add team", page.getTitle());

        page.click("Teams");
        page.click("List");
        assertEquals("List of teams", page.getTitle());

        page.click("Team members");
        page.click("Add");
        assertEquals("Add team member", page.getTitle());

        page.click("Team members");
        page.click("List");
        assertEquals("List of team members", page.getTitle());
    }

    @Test
    public void languageChangeTest() throws Exception {
        FrontPage page = site.getFrontPage(true);

        page.click("Language");
        page.click("Estonian");

        assertTrue(page.pageContains("Kodu"));

        page.click("Keel");
        page.click("Inglise");

        assertTrue(page.pageContains("Home"));
    }

}
