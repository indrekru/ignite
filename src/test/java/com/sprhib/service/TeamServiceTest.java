package com.sprhib.service;

import com.sprhib.dao.TeamDAO;
import com.sprhib.dao.TeamDAOImpl;
import com.sprhib.init.BaseTestConfig;
import com.sprhib.model.Team;
import com.sprhib.service.TeamService;
import com.sprhib.service.TeamServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * Created by interaal on 24.11.14.
 */
@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes=BaseTestConfig.class)
public class TeamServiceTest {

    @Mock
    TeamDAO teamDAO;

    @Test
    public void delete(){
        TeamService ts = new TeamServiceImpl(teamDAO);
        ts.deleteTeam(1);
        verify(teamDAO).deleteTeam(1);
    }

    @Test
    public void getById(){
        TeamService ts = new TeamServiceImpl(teamDAO);
        ts.getTeam(1);
        verify(teamDAO).getTeam(1);
    }

    @Test
    public void getTeams(){
        TeamService ts = new TeamServiceImpl(teamDAO);
        ts.getTeams();
        verify(teamDAO).getTeams();
    }

    @Test
    public void getTeamsIds(){
        TeamService ts = new TeamServiceImpl(teamDAO);
        ArrayList<Integer> ids = new ArrayList<Integer>() {{
            add(1);
        }};
        ts.getTeams(ids);
        verify(teamDAO).getTeams(ids);
    }

    @Test
    public void update(){
        TeamService ts = new TeamServiceImpl(teamDAO);
        Team team = new Team(1);
        ts.updateTeam(team);
        verify(teamDAO).updateTeam(team);
    }

    @Test
    public void add(){
        TeamService ts = new TeamServiceImpl(teamDAO);
        Team team = new Team(1);
        ts.addTeam(team);
        verify(teamDAO).addTeam(team);
    }

}
