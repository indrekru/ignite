package com.sprhib.service;

import com.sprhib.dao.MemberDAO;
import com.sprhib.dao.OrganizationDAO;
import com.sprhib.init.BaseTestConfig;
import com.sprhib.model.Member;
import com.sprhib.model.Organization;
import com.sprhib.service.MemberService;
import com.sprhib.service.MemberServiceImpl;
import com.sprhib.service.OrganizationService;
import com.sprhib.service.OrganizationServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.mockito.Mockito.verify;

/**
 * Created by interaal on 24.11.14.
 */
@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes=BaseTestConfig.class)
public class OrganizationServiceTest {

    @Mock
    OrganizationDAO organizationDAO;

    @Test
    public void delete(){
        OrganizationService ts = new OrganizationServiceImpl(organizationDAO);
        ts.deleteOrganization(1);
        verify(organizationDAO).deleteOrganization(1);
    }

    @Test
    public void getById(){
        OrganizationService ts = new OrganizationServiceImpl(organizationDAO);
        ts.getOrganization(1);
        verify(organizationDAO).getOrganization(1);
    }

    @Test
    public void getTeams(){
        OrganizationService ts = new OrganizationServiceImpl(organizationDAO);
        ts.getOrganizations();
        verify(organizationDAO).getOrganizations();
    }

    @Test
    public void update(){
        OrganizationService ts = new OrganizationServiceImpl(organizationDAO);
        Organization organization = new Organization(1);
        ts.updateOrganization(organization);
        verify(organizationDAO).updateOrganization(organization);
    }

    @Test
    public void add(){
        OrganizationService ts = new OrganizationServiceImpl(organizationDAO);
        Organization organization = new Organization(1);
        ts.addOrganization(organization);
        verify(organizationDAO).addOrganization(organization);
    }

}
