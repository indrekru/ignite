package com.sprhib.service;

import com.sprhib.dao.MemberDAO;
import com.sprhib.dao.TeamDAO;
import com.sprhib.init.BaseTestConfig;
import com.sprhib.model.Member;
import com.sprhib.model.Team;
import com.sprhib.service.MemberService;
import com.sprhib.service.MemberServiceImpl;
import com.sprhib.service.TeamService;
import com.sprhib.service.TeamServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;

import static org.mockito.Mockito.verify;

/**
 * Created by interaal on 24.11.14.
 */
@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes=BaseTestConfig.class)
public class MemberServiceTest {

    @Mock
    MemberDAO memberDAO;

    @Test
    public void deleteById(){
        MemberService ts = new MemberServiceImpl(memberDAO);
        ts.deleteMember(1);
        verify(memberDAO).deleteMember(1);
    }

    @Test
    public void getById(){
        MemberService ts = new MemberServiceImpl(memberDAO);
        ts.getMember(1);
        verify(memberDAO).getMember(1);
    }

    @Test
    public void getTeams(){
        MemberService ts = new MemberServiceImpl(memberDAO);
        ts.getMembers();
        verify(memberDAO).getMembers();
    }

    @Test
    public void update(){
        MemberService ts = new MemberServiceImpl(memberDAO);
        Member member = new Member(1);
        ts.updateMember(member);
        verify(memberDAO).updateMember(member);
    }

    @Test
    public void add(){
        MemberService ts = new MemberServiceImpl(memberDAO);
        Member member = new Member(1);
        ts.addMember(member);
        verify(memberDAO).addMember(member);
    }

}
