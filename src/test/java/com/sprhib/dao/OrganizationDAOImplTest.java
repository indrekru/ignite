package com.sprhib.dao;

import com.sprhib.init.BaseTestConfig;
import com.sprhib.model.Organization;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by indrekruubel on 28/11/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes=BaseTestConfig.class)
@Transactional
public class OrganizationDAOImplTest {

    @Autowired
    private OrganizationDAO organizationDAO;


    @Test
    public void saveOrganization() {

        // Test saving
        Organization organization = saveOrganizationReturn();
        assertNotNull(organization.getId());
        Organization organizationSaved = organizationDAO.getOrganization(organization.getId());
        assertEquals(organization, organizationSaved);
    }

    @Test
    public void editOrganization(){
        // Can't edit if no user. create one
        Organization organization = saveOrganizationReturn();

        // Test updating
        String renamed = "NameChanged";
        organization.setName(renamed);
        organizationDAO.updateOrganization(organization);
        Organization updatedOrganization = organizationDAO.getOrganization(organization.getId());
        assertEquals(updatedOrganization.getName(), renamed);
    }

    @Test
    public void deleteOrganization(){
        // Can't delete if no user. create one
        Organization organization = saveOrganizationReturn();

        // Test deleting
        organizationDAO.deleteOrganization(organization.getId());
        Organization deletedOrganization = organizationDAO.getOrganization(organization.getId());
        assertNull(deletedOrganization);
    }

    /**
     * Reusable method
     * @return
     */
    private Organization saveOrganizationReturn(){
        Organization organization = new Organization();
        organization.setName("TestOrganization");
        organizationDAO.addOrganization(organization);
        return organization;
    }

}
