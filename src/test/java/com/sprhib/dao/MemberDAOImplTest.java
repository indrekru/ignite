package com.sprhib.dao;

import com.sprhib.init.BaseTestConfig;
import com.sprhib.model.Member;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by indrekruubel on 28/11/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes=BaseTestConfig.class)
@Transactional
public class MemberDAOImplTest {

    @Autowired
    private MemberDAO memberDAO;

    @Test
    public void saveMember() {

        // Test saving
        Member member = saveMemberReturn();
        assertNotNull(member.getId());
        Member memberSaved = memberDAO.getMember(member.getId());
        assertEquals(member, memberSaved);
    }

    @Test
    public void editMember(){
        // Can't edit if no user. create one
        Member member = saveMemberReturn();

        // Test updating
        String renamed = "NameChanged";
        member.setName(renamed);
        memberDAO.updateMember(member);
        Member updatedMember = memberDAO.getMember(member.getId());
        assertEquals(updatedMember.getName(), renamed);
    }

    @Test
    public void deleteMember(){
        // Can't delete if no user. create one
        Member member = saveMemberReturn();

        // Test deleting
        memberDAO.deleteMember(member.getId());
        Member deletedMember = memberDAO.getMember(member.getId());
        assertNull(deletedMember);
    }

    /**
     * Reusable method
     * @return
     */
    private Member saveMemberReturn(){
        Member member = new Member();
        member.setName("TestMember");
        member.setRating(1);
        memberDAO.addMember(member);
        return member;
    }

}
