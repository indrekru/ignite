package com.sprhib.dao;

import com.sprhib.init.BaseTestConfig;
import com.sprhib.model.Member;
import com.sprhib.model.Team;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by indrekruubel on 28/11/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes=BaseTestConfig.class)
@Transactional
public class TeamDAOImplTest {

    @Autowired
    private TeamDAO teamDAO;

//    @Test
//    public void shouldSaveTeam() {
//
//        // Test saving
//        Team team = new Team();
//        team.setName("TestTeam");
//        team.setOrganization(null);
//        team.setRating(1);
//        teamDAO.addTeam(team);
//        assertNotNull(team.getId());
//        Team teamSaved = teamDAO.getTeam(team.getId());
//        assertEquals(team, teamSaved);
//
//        // Test updating
//        String renamed = "NameChanged";
//        team.setName(renamed);
//        teamDAO.updateTeam(team);
//        Team updatedTeam = teamDAO.getTeam(team.getId());
//        assertEquals(updatedTeam.getName(), renamed);
//
//        // Test deleting
//        teamDAO.deleteTeam(team.getId());
//        Team deletedTeam = teamDAO.getTeam(team.getId());
//        assertNull(deletedTeam);
//    }

    @Test
    public void saveTeam() {

        // Test saving
        Team team = saveTeamReturn();
        assertNotNull(team.getId());
        Team teamSaved = teamDAO.getTeam(team.getId());
        assertEquals(team, teamSaved);
    }

    @Test
    public void editTeam(){
        // Can't edit if no user. create one
        Team team = saveTeamReturn();

        // Test updating
        String renamed = "NameChanged";
        team.setName(renamed);
        teamDAO.updateTeam(team);
        Team updatedTeam = teamDAO.getTeam(team.getId());
        assertEquals(updatedTeam.getName(), renamed);
    }

    @Test
    public void deleteTeam(){
        // Can't delete if no user. create one
        Team team = saveTeamReturn();

        // Test deleting
        teamDAO.deleteTeam(team.getId());
        Team deletedTeam = teamDAO.getTeam(team.getId());
        assertNull(deletedTeam);
    }

    /**
     * Reusable method
     * @return
     */
    private Team saveTeamReturn(){
        Team team = new Team();
        team.setName("TestMember");
        team.setRating(1);
        teamDAO.addTeam(team);
        return team;
    }


}
