package com.sprhib.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by interaal on 22.11.14.
 */
@Entity
@Table(name="member")
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "rating")
    private Integer rating;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "team_member", joinColumns = {
            @JoinColumn(name = "fk_member") },
            inverseJoinColumns = { @JoinColumn(name = "fk_team")})
    private List<Team> teams;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public Integer getRating() {
        return rating;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public Member() {
    }

    public Member(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Member{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", rating=" + rating +
                ", teams=" + teams +
                '}';
    }
}

