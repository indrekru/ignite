package com.sprhib.model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="teams")
public class Team {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "rating")
	private Integer rating;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_organization", nullable = true)
	private Organization organization;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "team_member", joinColumns = {
			@JoinColumn(name = "fk_team") },
			inverseJoinColumns = { @JoinColumn(name = "fk_member")})
	private List<Member> members;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	@Override
	public String toString() {
		return "Team{" +
				"id=" + id +
				", name='" + name + '\'' +
				", rating=" + rating +
				'}';
	}

    public Team(){

    }

    public Team(Integer id) {
        this.id = id;
    }

}
