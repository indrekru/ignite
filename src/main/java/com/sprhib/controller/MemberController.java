package com.sprhib.controller;

import com.sprhib.model.Member;
import com.sprhib.model.Organization;
import com.sprhib.model.Team;
import com.sprhib.service.MemberService;
import com.sprhib.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.beans.PropertyEditorSupport;
import java.util.*;

/**
 * Created by interaal on 22.11.14.
 */
@Controller
@Transactional
@RequestMapping(value="/member")
public class MemberController {


    @Autowired
    private MemberService memberService;

    @Autowired
    private TeamService teamService;

    @InitBinder
    protected void initBinder(WebDataBinder binder){
        binder.registerCustomEditor(List.class, "teams", new CustomCollectionEditor(List.class) {
            @Override
            protected Object convertElement(Object element) {
                String id = (String) element;
                return new Team(Integer.parseInt(id));
            }
        });
    }

    @RequestMapping(value="/add", method= RequestMethod.GET)
    public ModelAndView addMemberPage() {
        ModelAndView modelAndView = new ModelAndView("add-member-form");

        List<Team> teams = teamService.getTeams();

        modelAndView.addObject("teams", teams);
        modelAndView.addObject("member", new Member());
        return modelAndView;
    }

    @RequestMapping(value="/add", method=RequestMethod.POST)
    public ModelAndView addingMember(HttpServletRequest request, @ModelAttribute Member member) {

        ModelAndView modelAndView = new ModelAndView("home");
        System.out.println(member);
        memberService.addMember(member);

        String message = "Member was successfully added.";
        modelAndView.addObject("message", message);

        return modelAndView;
    }

    @RequestMapping(value="/list")
    public ModelAndView listOfMembers() {
        ModelAndView modelAndView = new ModelAndView("list-of-members");

        List<Member> members = memberService.getMembers();
        modelAndView.addObject("members", members);

        return modelAndView;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public ModelAndView editMemberPage(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("edit-member-form");
        Member member = memberService.getMember(id);
        modelAndView.addObject("member", member);
        return modelAndView;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
    public ModelAndView edditingMember(@ModelAttribute Member member, @PathVariable Integer id) {

        ModelAndView modelAndView = new ModelAndView("home");

        memberService.updateMember(member);

        String message = "Member was successfully edited.";
        modelAndView.addObject("message", message);

        return modelAndView;
    }

    @RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
    public ModelAndView deleteOrganization(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("home");
        memberService.deleteMember(id);
        String message = "Member was successfully deleted.";
        modelAndView.addObject("message", message);
        return modelAndView;
    }


}
