package com.sprhib.controller;

import com.sprhib.model.Organization;
import com.sprhib.model.Team;
import com.sprhib.service.OrganizationService;
import com.sprhib.service.TeamService;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by interaal on 22.11.14.
 */
@Controller
@Transactional
@RequestMapping(value="/organization")
public class OrganizationController {

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private TeamService teamService;


    @RequestMapping(value="/add", method= RequestMethod.GET)
    public ModelAndView addTeamPage() {
        ModelAndView modelAndView = new ModelAndView("add-organization-form");
        modelAndView.addObject("organization", new Organization());
        return modelAndView;
    }

    @RequestMapping(value="/add", method=RequestMethod.POST)
    public ModelAndView addingTeam(@ModelAttribute Organization organization) {

        ModelAndView modelAndView = new ModelAndView("home");
        organizationService.addOrganization(organization);

        String message = "Organization was successfully added.";
        modelAndView.addObject("message", message);

        return modelAndView;
    }

    @RequestMapping(value="/list")
    public ModelAndView listOfOrganizations() {
        ModelAndView modelAndView = new ModelAndView("list-of-organizations");

        List<Organization> organizations = organizationService.getOrganizations();
        modelAndView.addObject("organizations", organizations);

        return modelAndView;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public ModelAndView editOrganizationPage(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("edit-organization-form");
        Organization organization = organizationService.getOrganization(id);
        modelAndView.addObject("organization", organization);
        return modelAndView;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
    public ModelAndView edditingOrganization(@ModelAttribute Organization organization, @PathVariable Integer id) {

        ModelAndView modelAndView = new ModelAndView("home");

        organizationService.updateOrganization(organization);

        String message = "Organization was successfully edited.";
        modelAndView.addObject("message", message);

        return modelAndView;
    }

    @RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
    public ModelAndView deleteOrganization(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("home");
        organizationService.deleteOrganization(id);

        String message = "Organization was successfully deleted.";
        modelAndView.addObject("message", message);
        return modelAndView;
    }

}
