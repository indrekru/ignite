package com.sprhib.service;

import com.sprhib.model.Member;
import com.sprhib.model.Team;

import java.util.List;

/**
 * Created by interaal on 22.11.14.
 */
public interface MemberService {

    public void addMember(Member member);
    public void updateMember(Member member);
    public Member getMember(int id);
    public void deleteMember(int id);
    public List<Member> getMembers();

}
