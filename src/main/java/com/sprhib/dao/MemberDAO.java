package com.sprhib.dao;

import com.sprhib.model.Member;

import java.util.List;

/**
 * Created by interaal on 22.11.14.
 */
public interface MemberDAO {
    public void addMember(Member member);
    public void updateMember(Member member);
    public Member getMember(int id);
    public void deleteMember(int id);
    public List<Member> getMembers();
}
