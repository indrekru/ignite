package com.sprhib.dao;

import com.sprhib.model.Organization;
import com.sprhib.model.Team;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public class OrganizationDAOImpl implements OrganizationDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void addOrganization(Organization organization) {
		getCurrentSession().save(organization);
	}

	@Override
	public void updateOrganization(Organization organization) {
		Organization organizationToUpdate = getOrganization(organization.getId());
		organizationToUpdate.setName(organization.getName());
		getCurrentSession().update(organizationToUpdate);
	}

	@Override
	public Organization getOrganization(int id) {
		return (Organization) getCurrentSession().get(Organization.class, id);
	}

	@Override
	public void deleteOrganization(int id) {
		Organization organization = getOrganization(id);
		if (organization != null) {
			List<Team> teams = organization.getTeams();
            if(teams != null) {
                for (Team team : teams) {
                    team.setOrganization(null);
                }
            }
			getCurrentSession().delete(organization);
		}
	}

	@Override
	public List<Organization> getOrganizations() {
		return getCurrentSession().createQuery("from Organization").list();
	}
}
