package com.sprhib.dao;

import com.sprhib.model.Member;
import com.sprhib.model.Organization;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by interaal on 22.11.14.
 */
@Repository
public class MemberDAOImpl implements MemberDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void addMember(Member member) {
        getCurrentSession().save(member);
    }

    @Override
    public void updateMember(Member member) {
        Member memberToUpdate = getMember(member.getId());
        memberToUpdate.setName(member.getName());
        memberToUpdate.setRating(member.getRating());
        getCurrentSession().update(memberToUpdate);
    }

    @Override
    public Member getMember(int id) {
        return (Member) getCurrentSession().get(Member.class, id);
    }

    @Override
    public void deleteMember(int id) {
        Member member = getMember(id);
        if (member != null)
            getCurrentSession().delete(member);
    }

    @Override
    public List<Member> getMembers() {
        return getCurrentSession().createQuery("from Member").list();
    }
}
