package com.sprhib.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by interaal on 29.11.14.
 */
abstract public class EditPage extends Page {

    @FindBy(id = "name")
    protected WebElement name;

    @FindBy(id = "rating")
    protected WebElement rating;

    @FindBy(id = "edit-button")
    protected WebElement editButton;

    public EditPage(WebDriver driver) {
        super(driver);
    }

    public void setName(String text){
        name.clear();
        name.sendKeys(text);
    }

    public void setRating(Integer number){
        rating.clear();
        rating.sendKeys(String.valueOf(number));
    }

    public void clickEdit(){
        editButton.click();
    }

}
