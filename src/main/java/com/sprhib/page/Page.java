package com.sprhib.page;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.Random;

/**
 * Created by interaal on 29.11.14.
 */
abstract public class Page {

    protected WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public int getRandom(){
        Random generator = new Random();
        int i = generator.nextInt(100) + 1;
        return i;
    }

    public String getTitle(){
        return driver.getTitle();
    }

    public boolean pageContains(String text){
        return driver.getPageSource().contains(text);
    }

    public void click(String text){
        driver.findElement(By.linkText(text)).click();
    }

}
