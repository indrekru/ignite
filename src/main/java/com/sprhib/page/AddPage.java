package com.sprhib.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by interaal on 29.11.14.
 */
abstract public class AddPage extends Page {

    @FindBy(id = "name")
    protected WebElement name;

    @FindBy(id = "rating")
    protected WebElement rating;

    @FindBy(id = "add-button")
    protected WebElement addButton;

    public AddPage(WebDriver driver) {
        super(driver);
    }

    public void setName(String text){
        name.sendKeys(text);
    }

    public void setRating(Integer number){
        rating.sendKeys(String.valueOf(number));
    }

    public void clickAdd(){
        addButton.click();
    }
}

