package com.sprhib.page;

import org.openqa.selenium.WebDriver;

/**
 * Created by interaal on 29.11.14.
 */
public class OrganizationPage extends Page {
    public OrganizationPage(WebDriver driver) {
        super(driver);
    }

    public OrganizationAddPage getOrganizationAddPage(boolean getUrl){
        if(getUrl)
            driver.get("http://localhost:8080/organization/add");
        return new OrganizationAddPage(driver);
    }

    public OrganizationListPage getOrganizationListPage(boolean getUrl){
        if(getUrl)
            driver.get("http://localhost:8080/organization/list");
        return new OrganizationListPage(driver);
    }

    public OrganizationEditPage getOrganizationEditPage(boolean getUrl){
        if(getUrl)
            driver.get("http://localhost:8080/organization/edit");
        return new OrganizationEditPage(driver);
    }
}
