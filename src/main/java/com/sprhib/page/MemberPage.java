package com.sprhib.page;

import org.openqa.selenium.WebDriver;

/**
 * Created by interaal on 29.11.14.
 */
public class MemberPage extends Page {

    public MemberPage(WebDriver driver) {
        super(driver);
    }

    public MemberAddPage getMemberAddPage(boolean getUrl) {
        if(getUrl)
            driver.get("http://localhost:8080/member/add");
        return new MemberAddPage(driver);
    }

    public MemberListPage getMemberListPage(boolean getUrl) {
        if(getUrl)
            driver.get("http://localhost:8080/member/list");
        return new MemberListPage(driver);
    }

    public MemberEditPage getMemberEditPage(boolean getUrl) {
        if(getUrl)
            driver.get("http://localhost:8080/member/edit");
        return new MemberEditPage(driver);
    }

}
