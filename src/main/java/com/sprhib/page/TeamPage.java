package com.sprhib.page;

import org.openqa.selenium.WebDriver;

/**
 * Created by interaal on 29.11.14.
 */
public class TeamPage extends Page {

    public TeamPage(WebDriver driver) {
        super(driver);
    }

    public TeamAddPage getTeamAddPage(boolean getUrl) {
        if(getUrl)
            driver.get("http://localhost:8080/team/add");
        return new TeamAddPage(driver);
    }

    public TeamListPage getTeamListPage(boolean getUrl) {
        if(getUrl)
            driver.get("http://localhost:8080/team/list");
        return new TeamListPage(driver);
    }

    public TeamEditPage getTeamEditPage(boolean getUrl) {
        if(getUrl)
            driver.get("http://localhost:8080/team/edit");
        return new TeamEditPage(driver);
    }

}
