package com.sprhib.page;

import org.openqa.selenium.WebDriver;

/**
 * Created by interaal on 29.11.14.
 */
public class Site {

    private WebDriver driver;

    public Site(WebDriver driver) {
        this.driver = driver;
    }

    public FrontPage getFrontPage(boolean getUrl) {
        if (getUrl)
            driver.get("http://localhost:8080/");
        return new FrontPage(driver);
    }

    public OrganizationPage getOrganizationPage(){
        return new OrganizationPage(driver);
    }

    public TeamPage getTeamPage(){
        return new TeamPage(driver);
    }

    public MemberPage getMemberPage(){
        return new MemberPage(driver);
    }

}
