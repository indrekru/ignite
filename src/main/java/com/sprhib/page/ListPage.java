package com.sprhib.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Iterator;
import java.util.List;

/**
 * Created by interaal on 29.11.14.
 */
abstract public class ListPage extends Page {

    @FindBy(id = "row")
    private List<WebElement> elements;

    public ListPage(WebDriver driver) {
        super(driver);
    }

    private List<WebElement> findLinkElements(String text){
        for (Iterator<WebElement> iterator = elements.iterator(); iterator.hasNext();) {
            WebElement webElement = iterator.next();
            List<WebElement> findElement = webElement.findElements(By.id("name-cell"));
            if( findElement.size() > 0 ){
                if( findElement.get(0).getText() != null && findElement.get(0).getText().indexOf(text) != -1 ) {
                    List<WebElement> aElement = webElement.findElements(By.xpath("./td/a"));
                    return aElement;
                }
            }
        }
        return null;
    }

    public void clickDeleteWithRowContaining(String text){
        List<WebElement> linkElements = findLinkElements(text);
        if(linkElements != null && linkElements.size() > 1) {
            linkElements.get(1).click();
        }
    }

    public void clickEditWithRowContaining(String text){
        List<WebElement> linkElements = findLinkElements(text);
        if(linkElements != null && linkElements.size() > 1) {
            linkElements.get(0).click();
        }
    }

}

