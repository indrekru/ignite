package com.sprhib.page;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by interaal on 29.11.14.
 */
public class MemberAddPage extends AddPage {
    public MemberAddPage(WebDriver driver) {
        super(driver);
    }

    private boolean existsElement(String id) {
        try {
            driver.findElement(By.id(id));
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    public void selectFirstTeam(){
        if(existsElement("select-teams")){
            new Select(driver.findElement(By.id("select-teams"))).selectByIndex(0);
        }
    }

}
