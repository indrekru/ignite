
INSERT INTO `hibnatedb`.`organizations` (`id`, `name`) VALUES (NULL, 'Ignite');

INSERT INTO `hibnatedb`.`teams` (`id`, `name`, `rating`, `fk_organization`) VALUES (NULL, 'Team1', '5', '1');
INSERT INTO `hibnatedb`.`teams` (`id`, `name`, `rating`, `fk_organization`) VALUES (NULL, 'Team2', '4', '1');

INSERT INTO `hibnatedb`.`member` (`id`, `name`, `rating`) VALUES (NULL, 'Toomas', '4');
INSERT INTO `hibnatedb`.`member` (`id`, `name`, `rating`) VALUES (NULL, 'Indrek', '5');

INSERT INTO `hibnatedb`.`team_member` (`fk_team`, `fk_member`) VALUES ('1', '1');
INSERT INTO `hibnatedb`.`team_member` (`fk_team`, `fk_member`) VALUES ('1', '2');
INSERT INTO `hibnatedb`.`team_member` (`fk_team`, `fk_member`) VALUES ('2', '1');