
CREATE TABLE `hibnatedb`.`organizations`(
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `hibnatedb`.`teams`(
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `rating` INT,
  `fk_organization` INT,
  PRIMARY KEY (`id`)
);

CREATE TABLE `hibnatedb`.`member`(
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `rating` INT,
  PRIMARY KEY (`id`)
);

CREATE TABLE `hibnatedb`.`team_member`(
  `fk_team` INT NOT NULL,
  `fk_member` INT NOT NULL,
  PRIMARY KEY (`fk_team`, `fk_member`)
);