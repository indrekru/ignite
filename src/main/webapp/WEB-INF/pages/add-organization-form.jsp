<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="../components/metas.jsp" />

    <title>Add organization</title>

    <jsp:include page="../components/top_includes.jsp" />

</head>

<body role="document">

<jsp:include page="../components/navbar.jsp" />

<div class="container theme-showcase" role="main">

    <h1><spring:message code="organizations.add.page.title" /></h1>
    <p><spring:message code="organizations.add.page.description" /></p>
    <form:form method="POST" commandName="organization" action="${pageContext.request.contextPath}/organization/add.html">
      <table>
        <tbody>
        <tr>
          <td><spring:message code="organizations.add.page.name.label" />:</td>
          <td><form:input path="name" required="required" /></td>
        </tr>
        <tr>
          <td><input type="submit" id="add-button" value="<spring:message code="organizations.add.page.add.label" />" /></td>
          <td></td>
        </tr>
        </tbody>
      </table>
    </form:form>

</div>
<jsp:include page="../components/bottom_includes.jsp" />
</body>
</html>