<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="../components/metas.jsp" />

    <title>List of organizations</title>

    <jsp:include page="../components/top_includes.jsp" />

</head>

<body role="document">

<jsp:include page="../components/navbar.jsp" />

<div class="container theme-showcase" role="main">

    <h1><spring:message code="organizations.list.page.title" /></h1>
    <p><spring:message code="organizations.list.page.description" /></p>
    <table border="1px" cellpadding="0" id="org-table" cellspacing="0" >
    <thead>
    <tr>
        <th width="10%"><spring:message code="organizations.list.page.id.label" /></th>
        <th width="15%"><spring:message code="organizations.list.page.name.label" /></th>
        <th width="10%"><spring:message code="organizations.list.page.actions.label" /></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="organization" items="${organizations}">
    <tr id="row">
        <td>${organization.id}</td>
        <td id="name-cell">${organization.name}</td>
        <td>
        <a href="${pageContext.request.contextPath}/organization/edit/${organization.id}.html"><spring:message code="organizations.list.page.edit.label" /></a><br/>
        <a href="${pageContext.request.contextPath}/organization/delete/${organization.id}.html"><spring:message code="organizations.list.page.delete.label" /></a><br/>
        </td>
    </tr>
    </c:forEach>
    </tbody>
    </table>

</div>
<jsp:include page="../components/bottom_includes.jsp" />
</body>
</html>