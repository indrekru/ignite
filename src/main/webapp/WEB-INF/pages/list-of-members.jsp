<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="../components/metas.jsp" />

    <title>List of team members</title>

    <jsp:include page="../components/top_includes.jsp" />

</head>

<body role="document">

<jsp:include page="../components/navbar.jsp" />

<div class="container theme-showcase" role="main">

    <h1><spring:message code="team_members.list.page.title" /></h1>
    <p><spring:message code="team_members.list.page.description" /></p>
    <table border="1px" cellpadding="0" cellspacing="0" >
    <thead>
    <tr>
        <th width="10%"><spring:message code="team_members.list.page.id.label" /></th>
        <th width="15%"><spring:message code="team_members.list.page.name.label" /></th>
        <th width="10%"><spring:message code="team_members.list.page.rating.label" /></th>
        <th width="10%"><spring:message code="team_members.list.page.actions.label" /></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="member" items="${members}">
    <tr id="row">
        <td>${member.id}</td>
        <td id="name-cell">${member.name}</td>
        <td>${member.rating}</td>
        <td>
        <a href="${pageContext.request.contextPath}/member/edit/${member.id}.html"><spring:message code="team_members.list.page.edit.label" /></a><br/>
        <a href="${pageContext.request.contextPath}/member/delete/${member.id}.html"><spring:message code="team_members.list.page.delete.label" /></a><br/>
        </td>
    </tr>
    </c:forEach>
    </tbody>
    </table>

</div>
<jsp:include page="../components/bottom_includes.jsp" />
</body>
</html>