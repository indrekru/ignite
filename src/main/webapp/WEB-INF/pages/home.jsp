<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="../components/metas.jsp" />

    <title>Ignite test assignment by Indrek Ruubel</title>

    <jsp:include page="../components/top_includes.jsp" />

</head>

<body role="document">

<jsp:include page="../components/navbar.jsp" />

<div class="container theme-showcase" role="main">

    <h1><spring:message code="home.page.title" /></h1>
    <c:if test="${not empty message}">
        <div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
            <%--<span class="sr-only">Error:</span>--%>
            ${message}
        </div>
    </c:if>
    <p>
        <spring:message code="home.page.welcome_text" />
    </p>

</div>
<jsp:include page="../components/bottom_includes.jsp" />
</body>
</html>
