<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="../components/metas.jsp" />

    <title>Add team</title>

    <jsp:include page="../components/top_includes.jsp" />

</head>

<body role="document">

<jsp:include page="../components/navbar.jsp" />

<div class="container theme-showcase" role="main">

    <h1><spring:message code="teams.add.page.title" /></h1>
    <p><spring:message code="teams.add.page.description" /></p>
    <form:form method="POST" commandName="team" action="${pageContext.request.contextPath}/team/add.html">
    <table>
    <tbody>
        <tr>
            <td><spring:message code="teams.add.page.name.label" />:</td>
            <td><form:input path="name" id="name" required="required" /></td>
        </tr>
        <tr>
            <td><spring:message code="teams.add.page.rating.label" />:</td>
            <td><form:input path="rating" id="rating" required="required" pattern="\d*" /></td>
        </tr>
        <tr>
            <td><spring:message code="teams.add.page.organization.label" />:</td>
            <td>
                <c:if test="${not empty organizations}">
                <form:select
                    path="organization.id"
                    items="${organizations}"
                    itemLabel="name"
                    itemValue="id"
                />
                </c:if>
                <c:if test="${empty organizations}">
                    No organizations available
                </c:if>
            </td>
        </tr>
        <tr>
            <td><input type="submit" id="add-button" value="<spring:message code="teams.add.page.add.label" />" /></td>
            <td></td>
        </tr>
    </tbody>
    </table>
    </form:form>

</div>
<jsp:include page="../components/bottom_includes.jsp" />
</body>
</html>