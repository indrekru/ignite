<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="../components/metas.jsp" />

    <title>Edit organization</title>

    <jsp:include page="../components/top_includes.jsp" />

</head>

<body role="document">

<jsp:include page="../components/navbar.jsp" />

<div class="container theme-showcase" role="main">

    <h1><spring:message code="organizations.edit.page.title" /></h1>
    <p><spring:message code="organizations.edit.page.description" /></p>
    <p>${message}</p>
    <form:form method="POST" commandName="organization" action="${pageContext.request.contextPath}/organization/edit/${organization.id}.html">
    <table>
    <tbody>
        <tr>
            <td><spring:message code="organizations.edit.page.name.label" />:</td>
            <td><form:input path="name" id="name" required="required" /></td>
        </tr>
        <tr>
            <td><input type="submit" id="edit-button" value="<spring:message code="organizations.edit.page.edit.label" />" /></td>
            <td></td>
        </tr>
    </tbody>
    </table>
    </form:form>

</div>
<jsp:include page="../components/bottom_includes.jsp" />
</body>
</html>