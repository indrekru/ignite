spr-mvc-hib
===========

Spring MVC + Hibernate + Maven tutorial with all CRUD operations

building
===========

* First apply setup.db.sql to your database, along the lines:
mysql -uroot -p < setup_db.sql

* compile
* Run on tomcat
* Run tests (Server needs to be running for the tests to succeed)